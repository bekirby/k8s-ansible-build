# k8s-ansible-build
Kubernetes cluster build using either KVM/QEMU or VirtualBox VMs on Ubuntu.

## This ansible playbook will:
- download Ubuntu cloud image
- create multiple VMs for control-plane, workers, loadbalancer
- install Kubernetes packages on VMs
- initialize Kubernetes master node(s) - default is single control-plane node
- join worker node(s)
- install Calico networking
- install NGINX ingress
- configure kubectl on local workstation
- NOTE: Assumes VMs are on the same machine where Ansible is run 

## Prerequisites for build/control computer KVM/QEMU (default):
- git v2.34.1+
- ansible v2.10.8+
- apt install cloud-utils qemu-kvm libvirt-clients libvirt-daemon-system bridge-utils qemu-utils virtinst
- configure network bridge > https://www.tecmint.com/create-network-bridge-in-ubuntu/ 

## Prerequisites for build/control computer VirtualBox:
- git v2.34.1+
- ansible v2.10.8+
- VirtualBox v6.1.x+
- cloud-image-utils (cloud-utils)
- configure network bridge > https://www.tecmint.com/create-network-bridge-in-ubuntu/ 

## Steps
- clone repository - git clone https://gitlab.com/bekirby/k8s-ansible-build.git
- cd k8s-ansible-build/ansible/
- edit inv/group_vars/all.yml (see comments in file)
- edit inv/hosts.yml (see comments in file)
- edit files/network-config.yml.j2 (see comments in file)
- edit files/user-data.yml.j2 (see comments in file)
- run:

```ansible-playbook -i inv/hosts.yml 00_cluster_build.yml``` 

OR 

```ansible-playbook -i inv/hosts.yml 00_cluster_build_virtualbox.yml```

## Extras
- 200_kvm_create_mount_disks.yml - Create and mount additional disks to worker KVMs
